//@ todo calculator
document.addEventListener("DOMContentLoaded", ready);

function ready() {
    var firstVal = document.getElementById('first-value');
    var secondVal = document.getElementById('second-value');
    var resultVal = document.getElementById('result');

    var operators = document.querySelectorAll('.operator');
    var calculate = document.querySelector('.btn-calc');

    var action;

    for (var i = 0; i < operators.length; i++) {
        operators[i].addEventListener('click', function(ev) {
            ev.preventDefault();
            action = this.getAttribute('href');
        });
    }


    calculate.addEventListener('click', function(ev) {
        ev.preventDefault();
        switch (action) {
            case "+":
                resultVal.value = +firstVal.value + (+secondVal.value);
                message(resultVal.value);
                break;
            case "-":
                resultVal.value = +firstVal.value - (+secondVal.value);
                message(resultVal.value);
                break;
            case "*":
                resultVal.value = +firstVal.value * (+secondVal.value);
                message(resultVal.value);
                break;
            case "/":
                resultVal.value = +firstVal.value / (+secondVal.value);
                message(resultVal.value);
                break;
        }
    });
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function message(n) {
    if (!isNumeric(n)) { alert('Not valid namber') }
}