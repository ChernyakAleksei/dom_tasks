(function() {
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function compareNumeric(a, b) {
        if (a > b) return 1;
        if (a < b) return -1;
    }

    function sortHandler() {
        //@todo отобразить в html исходный массив из 10 элементов, полученный при помощи getRandomInt
        //и отсортированный в порядке возрастания
        var div = document.getElementById('sort');
        var numberArr = [];
        var p = document.createElement('p');
        var pSort = document.createElement('p');
        for (var i = 0; i < 10; i++) {
            var number = getRandomInt(1, 100);
            numberArr.push(number);
        }
        p.innerText = numberArr;
        div.appendChild(p);
        numberArr.sort(compareNumeric);

        pSort.innerText = numberArr;
        div.appendChild(pSort);


    }


    var arrlist = [1, 2, [3, 4, [6, 7, 8], 9], 10, 11];
    var divlist = document.getElementById('list');

    function recursiveList(data) {
        //@todo отобразить все элементы массива массивов в виде вложенных списков соблюдая вложенность

        var ul = document.createElement('ul');

        for (var i = 0; i < data.length; i++) {
            var li = document.createElement('li');
            if (Array.isArray(data[i])) {
                li.appendChild(recursiveList(data[i]));
            } else {
                li.innerHTML = data[i];
            }
            ul.appendChild(li);
        }


        return ul;

    }
    var mylist = recursiveList(arrlist);

    divlist.appendChild(mylist);


    var divlist = document.getElementById('headings');
    var fragment = document.createDocumentFragment();

    function recursiveHeadings(data, weight) {
        weight = (weight == undefined) ? 1 : weight;
        //var counter = weight;
        //@todo отобразить все элементы массива массивов в заголовков разного порядка в зависимости от уровня вложенности
        //исходный массив [1,2,[3,4,[6,7,8],9],10,11]
        var fragment = document.createDocumentFragment();
        for (var i = 0; i < data.length; i++) {
            var h = document.createElement('h' + weight);
            if (Array.isArray(data[i])) {
                weight++;
                fragment.appendChild(recursiveHeadings(data[i], weight));
            } else {
                h.innerHTML = data[i];
            }
            fragment.appendChild(h);
        }

        return fragment;
    }
    var mylist = recursiveHeadings([1, 2, [3, 4, [6, 7, 8], 9], 10, 11], 1);
    divlist.appendChild(mylist);


    var form = document.getElementById('form');
    var elem = document.querySelector('button');

    function simpleValidation(form) {
        //elem.addEventListener('submit', function (ev) { ev.preventDefault(); ...});
        //@todo при сабмите формы проверять поля на пустотое значение.
        //При ошибке подсвечивать красным соответствующее поле.
        //Если оба поля заполнены, вывести сообщение об удачной отправке формы
        elem.addEventListener('click', function(ev) {
            ev.preventDefault();
            var err = false;
            var inputs = form.getElementsByTagName('input');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].value == '') {
                    inputs[i].style.borderColor = 'red';
                    err = true;
                } else {
                    inputs[i].style.borderColor = 'green';
                }
            }
            if (!err) {
                alert('Form was sended');
            }

        });

    }
    simpleValidation(form);
    //вызывать функции здесь!
    sortHandler();
})();