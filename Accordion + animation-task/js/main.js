//todo  реализовать анимацию - при нажатии на старт ширина элемента увеличивается, при нажатии на стоп "рост" элемента останавливается
//реализовать аккордеон - при нажатии соответствующей вкладки ее содержимое раскрывается, а содержимое остальных - скрывается
document.addEventListener("DOMContentLoaded", ready);

function ready() {
    var box = document.getElementsByClassName('box')[0];
    var start = document.getElementsByClassName('start btn')[0];
    var stop = document.getElementsByClassName('stop btn')[0];
    var maxWidth = 800;
    var nowWidth = parseFloat(getComputedStyle(box).width);
    var timer;

    function draw(nowWidth) {
        box.style.width = nowWidth + 2 + 'px';

    }

    start.addEventListener('click', startAnimation);

    stop.addEventListener('click', stopAnimation);


    function startAnimation(ev) {
        ev.preventDefault();

        clearInterval(timer);

        timer = setInterval(function() {

            nowWidth = parseFloat(getComputedStyle(box).width);
            if (nowWidth >= maxWidth) {
                clearInterval(timer);
                return;
            }
            draw(nowWidth);
        }, 20);
    }

    function stopAnimation(ev) {
        clearInterval(timer);
        //start.removeEventListener(startAnimation);

    }

    (function() {

        var accordion = document.getElementById('accordion');
        var sections = document.querySelectorAll('#accordion > li');

        function opensubmenu(el) {
            el.style.height = 'auto';
            for (var i = 0; i < sections.length; i++) {
                if (sections[i].getAttribute('class') == 'active') {
                    var section = sections[i].children[1];
                    if (section == el) {
                        continue;
                    }
                    sections[i].setAttribute('class', '');
                    closesubmenu(section);
                }
            }

        }

        function closesubmenu(el) {
            el.style.height = '0px';
        }

        for (var i = 0; i < sections.length; i++) {
            (function() {
                var section = sections[i],
                    anchorLinc = sections[i].children[0],
                    submenu = sections[i].children[1];

                closesubmenu(submenu);

                anchorLinc.addEventListener('click', openSubmenu);

                function openSubmenu() {
                    if (section.getAttribute('class') == 'active') {
                        section.setAttribute('class', '');
                        closesubmenu(submenu);
                    } else {
                        section.setAttribute('class', 'active');
                        opensubmenu(submenu);
                    }
                }
            })();
        }
    })();
}